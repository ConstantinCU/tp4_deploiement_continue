# Chemins
root_dir="/builds/$GITLAB_USER_LOGIN/tp4_deploiement_continue"
env_dir="$root_dir/environment-test"
terraform_dir="$env_dir/terraform"
ansible_dir="$env_dir/ansible"

cd $terraform_dir
test_ip=$(terraform output test_instance_external_ip | sed 's/"//g')
user=$(terraform output instance_user | sed 's/"//g')

# Génération de l'inventaire avec les adresses IP
echo "[test]"
echo $test_ip ansible_user=$user
