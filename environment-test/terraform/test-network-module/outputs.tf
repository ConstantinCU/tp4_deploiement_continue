output "network_id_test" {
  value = google_compute_network.network_test.id
}

output "subnetwork_id_test" {
  value = google_compute_subnetwork.subnetwork_test.id
}