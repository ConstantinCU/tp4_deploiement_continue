# instance vm dev
resource "google_compute_instance" "dev_instance" {
  name                      = "dev-instance"
  machine_type              = "g1-small"
  zone                      = "us-east1-b"
  tags                      = ["server-dev"]
  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
    }
  }

  network_interface {
    network    = var.network_id_dev
    subnetwork = var.subnetwork_id_dev
    access_config {
      // necessary even empty
    }
  }

  metadata = {
    "ssh-keys" = "${var.user}:${file("~/.ssh/id_rsa.pub")}"
  }
}
