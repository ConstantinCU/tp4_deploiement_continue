variable "network_id_dev" {
  type = string
}

variable "subnetwork_id_dev" {
  type = string
}

variable "user" {
  default     = "aroutioun.tsakanian"
  description = "Nom de l'utilisateur"
  type        = string
}